open Jest;

module HttpBin = {
  module Slide = {
    [@bs.deriving abstract]
    type t = {
      title: string,
      [@bs.as "type"]
      type_: string,
      [@bs.optional]
      items: array(string),
    };
  };

  module Slideshow = {
    [@bs.deriving abstract]
    type t = {
      author: string,
      date: string,
      slides: array(Slide.t),
    };
  };

  module JsonRecord = {
    [@bs.deriving abstract]
    type t = {slideshow: Slideshow.t};

    [@bs.scope "JSON"] [@bs.val] external parse : string => t = "";
  };
};

let verifyAuthor = (data) =>
  data
  |. HttpBin.JsonRecord.parse
  |. HttpBin.JsonRecord.slideshowGet
  |. HttpBin.Slideshow.authorGet
  |. Expect.expect
  |> Expect.toBe("Yours Truly");

describe("Hyperquest", () => {
  let uri = "http://httpbin.org/json";

  describe("GET", () => {
    testAsync("Fetch httpbin.org JSON", finish =>
      Hyperquest.get(~uri, ())
      |. Hyperquest.on(
           `response(
             res => {
               let data = ref("");
               res
               |. Hyperquest.Msg.on(
                    `data(
                      b => data := data^ ++ Hyperquest.Buffer.toString(b),
                    ),
                  )
               |. Hyperquest.Msg.on(
                    `end_(_ => data^ |. verifyAuthor |. finish)
                  )
               |. Hyperquest.Msg.on(
                    `error(exn => exn |. Js.String.make |. fail |. finish),
                  )
               |. ignore;
             },
           ),
         )
      |. ignore
    );
    testPromise("Fetch wrapped in a promise", () => {
      let result =
        Js.Promise.make((~resolve, ~reject) =>
          Hyperquest.get(~uri, ())
          |. Hyperquest.on(
               `response(
                 res => {
                   let data = ref("");

                   res
                   |. Hyperquest.Msg.on(
                        `data(
                          b => data := data^ ++ Hyperquest.Buffer.toString(b),
                        ),
                      )
                   |. Hyperquest.Msg.on(
                        `error(exn => reject(. exn)),
                      )
                   |. Hyperquest.Msg.on(
                        `end_(_ => resolve(. data^)),
                      )
                   |. ignore;
                 },
               ),
             )
          |. ignore
        );

      result
      |> Js.Promise.then_(res => res |. verifyAuthor |. Js.Promise.resolve)
    });
  });

  describe("Future", () => {

    testAsync("Fetch httpbin.org JSON", finish =>
      Hyperquest.Future.get(~uri, ())
      |. Future.mapError(res =>
        switch(res) {
        | (_, `error(exn)) => exn |. Js.String.make |. fail |. finish
        | (_, `response(data)) => data |. fail |. finish
        }
      )
      |. Future.mapOk(((_, data)) => data |. verifyAuthor |. finish)
      |. ignore
    );
  });
});
