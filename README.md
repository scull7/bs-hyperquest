# bs-hyperquest

BuckleScript bindings for substack's [hyperquest] library.



## Usage

### Standard Event Interface

#### GET
```reason
let result =
  Js.Promise.make((~resolve, ~reject) =>
    Hyperquest.get(~uri, ())
    |. Hyperquest.on(
         `response(
           res => {
             let data = ref("");

             res
             |. Hyperquest.IncomingMessage.on(
                  `data(
                    b => data := data^ ++ Hyperquest.Buffer.toString(b),
                  ),
                )
             |. Hyperquest.IncomingMessage.on(
                  `error(exn => reject(. exn)),
                )
             |. Hyperquest.IncomingMessage.on(
                  `end_(_ => resolve(. data^)),
                )
             |. ignore;
           },
         ),
       )
    |. ignore
  );
```

### [reason-future] Interface

#### GET
```reason
Hyperquest.Future.get(~uri, ())
|. Future.mapOk(((_, data)) => data)
|. Future.mapError(res =>
  switch(res) {
  | (_, `error(exn)) => exn |. Js.String.make |. fail |. finish
  | (_, `response(data)) => data |. fail |. finish
  }
)
|. ignore
```

[hyperquest]: https://github.com/substack/hyperquest
[reason-future]: https://github.com/RationalJS/future
