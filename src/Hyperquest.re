module Buffer = Hyperquest_buffer;
module Callback = Hyperquest_callback;
module Msg = Hyperquest_incoming_message;
module Method = Hyperquest_method;
module Options = Hyperquest_options;
module Request = Hyperquest_request;
module Status = Hyperquest_status;

type t;

[@bs.module]
external make :
  (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t =
  "hyperquest";

[@bs.module "hyperquest"]
external get :
  (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t =
  "get";

[@bs.module "hyperquest"]
external post :
  (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t =
  "post";

[@bs.module "hyperquest"]
external put :
  (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t =
  "put";

[@bs.module "hyperquest"]
external delete :
  (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t =
  "delete";

[@bs.send] external end_ : t => unit = "end";
[@bs.send] external endWriteBuffer : (t, Buffer.t) => unit = "end";
[@bs.send]
external endWriteString : (t, string, [@bs.string] [ | `utf8 | `ascii]) => unit =
  "end";
[@bs.send] external endCb : (t, unit => unit) => unit = "end";
[@bs.send]
external endWriteBufferCb : (t, Buffer.t, unit => unit) => unit = "end";
[@bs.send]
external endWriteStringCb :
  (t, string, [@bs.string] [ | `utf8 | `ascii], unit => unit) => unit =
  "end";

let finish = t => end_(t);

let finishCb = (t, ~callback) => endCb(t, callback);

let finishWriteString = (t, string, ~encoding=`utf8, ~callback=?, _) =>
  switch (callback) {
  | Some(cb) => endWriteStringCb(t, string, encoding, cb)
  | None => endWriteString(t, string, encoding)
  };

let finishWriteBuffer = (t, buf, ~callback=?, _) =>
  switch (callback) {
  | Some(cb) => endWriteBufferCb(t, buf, cb)
  | None => endWriteBuffer(t, buf)
  };

[@bs.send]
external on :
  (
    t,
    [@bs.string] [
      | `request(Request.t => unit)
      | `response(Msg.t => unit)
      | `error(exn => unit)
    ]
  ) =>
  t =
  "";

[@bs.send] external setHeader : (t, string, string) => t = "";

[@bs.send] external setLocation : (t, string) => t = "";

[@bs.send]
external write :
  (
    t,
    [@bs.unwrap] [ | `String(string) | `Buffer(Buffer.t)],
    [@bs.string] [ | `utf8 | `ascii]
  ) =>
  bool =
  "";

[@bs.send]
external writeCb :
  (
    t,
    [@bs.unwrap] [ | `String(string) | `Buffer(Buffer.t)],
    [@bs.string] [ | `utf8 | `ascii],
    unit => unit
  ) =>
  bool =
  "write";

let writeString = (t, string, ~encoding=`utf8, ~callback=?, _) =>
  switch (callback) {
  | Some(cb) => writeCb(t, `String(string), encoding, cb)
  | None => write(t, `String(string), encoding)
  };

let writeBuffer = (t, buffer, ~callback=?, _) =>
  switch (callback) {
  | Some(cb) => writeCb(t, `Buffer(buffer), `utf8, cb)
  | None => write(t, `Buffer(buffer), `utf8)
  };

module Future =
  Hyperquest_future.Make({
    /* https://discuss.ocaml.org/t/how-to-achieve-type-t-t/1449 */
    type nonrec t = t;
    let make = make;
    let get = get;
    let post = post;
    let put = put;
    let delete = delete;
    let on = on;
  });
