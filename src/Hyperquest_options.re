module Method = Hyperquest_method;

[@bs.deriving abstract]
type t = {
  method: string,
  [@bs.optional]
  headers: Belt.Map.String.t(string),
  [@bs.optional]
  agent: string,
  [@bs.optional]
  auth: string,
  [@bs.optional]
  timeout: int,
};

let make = (
  ~method,
  ~headers=?,
  ~agent=?,
  ~auth=?,
  ~timeout=?,
  _
) => t(
  ~method=Method.toString(method),
  ~headers?,
  ~agent?,
  ~auth?,
  ~timeout?,
  (),
)
