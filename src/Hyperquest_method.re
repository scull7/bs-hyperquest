exception Method_unsupported(string);

type t =
  | Get
  | Post
  | Put
  | Delete
  | Head
  | Options;

let toString =
  fun
  | Get => "GET"
  | Post => "POST"
  | Put => "PUT"
  | Delete => "DELETE"
  | Head => "HEAD"
  | Options => "OPTIONS";

let fromStringUnsafe = string =>
  switch (string |. String.uppercase) {
  | "GET" => Get
  | "POST" => Post
  | "PUT" => Put
  | "DELETE" => Delete
  | "HEAD" => Head
  | "OPTIONS" => Options
  | x => x |. Method_unsupported |. raise
  };

let fromString = string =>
  try (string |. fromStringUnsafe |. Belt.Result.Ok) {
  | Method_unsupported(method) =>
    method |. Method_unsupported |. Belt.Result.Error
  };
