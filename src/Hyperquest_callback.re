module Msg = Hyperquest_incoming_message;

type t = (exn, Msg.t) => unit;
