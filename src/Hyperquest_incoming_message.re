module Buffer = Hyperquest_buffer;
module Client = Hyperquest_client;
module Connection = Hyperquest_connection;
module Socket = Hyperquest_socket;
module Status = Hyperquest_status;

[@bs.deriving abstract]
type t = {
  aborted: bool,
  client: Client.t,
  complete: bool,
  connection: Connection.t,
  headers: Belt.Map.String.t(string),
  httpVersionMajor: int,
  httpVersionMinor: int,
  httpVersion: string,
  readable: bool,
  rawHeaders: array(string),
  rawTrailers: array(string),
  socket: Socket.t,
  statusCode: int,
  statusMessage: string,
  [@bs.optional]
  trailers: Belt.Map.String.t(string),
  upgrade: bool,
  writable: bool,
};

[@bs.send]
external on :
  (
    t,
    [@bs.string] [
      | `aborted(unit => unit)
      | `close(unit => unit)
      | `data(Buffer.t => unit)
      | [@bs.as "end"] `end_(unit => unit)
      | `error(exn => unit)
    ]
  ) =>
  t =
  "";

let status = t => t |. statusCodeGet |. Status.fromCode;

let isSuccess = t => t |. statusCodeGet |. (x => x / 100 < 4);

let read = (t, onComplete, onError) => {
  let data = ref("");

  t
  |. on(`data(b => data := data^ ++ Buffer.toString(b)))
  |. on(`end_(_ => onComplete(t, data^)))
  |. on(`error(exn => onError(t, exn)));
};
