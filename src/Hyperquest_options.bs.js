// Generated by BUCKLESCRIPT VERSION 4.0.2, PLEASE EDIT WITH CARE
'use strict';

var Js_primitive = require("bs-platform/lib/js/js_primitive.js");
var Hyperquest_method = require("./Hyperquest_method.bs.js");

function make(method_, headers, agent, auth, timeout, _) {
  var tmp = {
    method: Hyperquest_method.toString(method_)
  };
  if (headers !== undefined) {
    tmp.headers = Js_primitive.valFromOption(headers);
  }
  if (agent !== undefined) {
    tmp.agent = Js_primitive.valFromOption(agent);
  }
  if (auth !== undefined) {
    tmp.auth = Js_primitive.valFromOption(auth);
  }
  if (timeout !== undefined) {
    tmp.timeout = Js_primitive.valFromOption(timeout);
  }
  return tmp;
}

var Method = 0;

exports.Method = Method;
exports.make = make;
/* No side effect */
