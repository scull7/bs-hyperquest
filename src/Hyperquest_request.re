module Agent = Hyperquest_agent;
module Buffer = Hyperquest_buffer;
module Connection = Hyperquest_connection;
module Msg = Hyperquest_incoming_message;
module Method = Hyperquest_method;
module Socket = Hyperquest_socket;

/**
 * ## http.ClientRequest
 *
 * ### Omitted Properties
 * * [socket] - Probably not what you want anyways.
 *
 * [socket]: https://nodejs.org/dist/latest-v8.x/docs/api/http.html#http_request_socket
 */

[@bs.deriving abstract]
type t = {
  [@bs.optional]
  aborted: int,
  agent: Agent.t,
  chunkedEncoding: bool,
  connection: Socket.t,
  finished: bool,
  [@bs.optional]
  maxHeadersCount: int,
  method: string,
  output: array(string),
  outputEncodings: array(string),
  outputSize: int,
  path: string,
  [@bs.optional]
  socketPath: string,
  shouldKeepAlive: bool,
  upgradeOrConnect: bool,
  writable: bool,
};

[@bs.send]
external on :
  (
    t,
    [@bs.string] [
      | `abort(unit => unit)
      | `connect((Msg.t, Socket.t, Buffer.t) => unit)
      | `continue(unit => unit)
      | `response(Msg.t => unit)
      | `socket(Socket.t => unit)
      | `timeout(unit => unit)
      | `upgrade((Msg.t, Socket.t, Buffer.t) => unit)
      | `drain(unit => unit)
    ]
  ) =>
  t =
  "";


[@bs.send] external abort : t => t = "";

[@bs.send] external end_ :
  (
    t, 
    [@bs.unwrap] [ | `String(string) | `Buffer(Buffer.t) ],
    [@bs.string] [ | `utf8 | `ascii ],
    option(unit => unit),
  ) => t = "end";

[@bs.send] external flushHeaders : t => t = "";

[@bs.send] external getHeader : (t, string) => option(string) = "";

[@bs.send] external removeHeader : (t, string) => t = "";

[@bs.send] external setHeader : (t, string, string) => t = "";

[@bs.send] external setHeaderMany : (t, string, array(string)) => t = "";

[@bs.send] external setNoDelay : t => t = "";

[@bs.send] external setSocketKeepAlive : (t, bool, int) => t = "";

[@bs.send] external setTimeout : (t, int, (unit => unit)) => t = "";

[@bs.send] external write :
  (
    [@bs.unwrap] [ | `String(string) | `Buffer(Buffer.t) ],
    [@bs.string] [ | `utf8 | `ascii ],
    option(unit => unit),
  ) => bool = "";

let methodGet = t => t |. methodGet |. Method.fromString;
