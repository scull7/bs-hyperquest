[@bs.deriving abstract]
type t = {
  defaultPort: int,
  protocol: string,
  keepAliveMsecs: int,
  keepAlive: bool,
  maxSockets: int,
  maxFreeSockets: int,
};
