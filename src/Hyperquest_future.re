module Callback = Hyperquest_callback;
module Msg = Hyperquest_incoming_message;
module Options = Hyperquest_options;
module Request = Hyperquest_request;

module type RequestTypeClass = {
  type t;

  let make:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t;

  let get: (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t;

  let post:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t;

  let put: (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t;

  let delete:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t;

  let on:
    (
      t,
      [
        | `request(Request.t => unit)
        | `response(Msg.t => unit)
        | `error(exn => unit)
      ]
    ) =>
    t;
};

module type FutureTypeClass = {
  type response =
    Future.t(
      Belt.Result.t(
        (Msg.t, string),
        (option(Msg.t), [ | `response(string) | `error(exn)]),
      ),
    );

  let make:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => response;

  let get:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => response;

  let post:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => response;

  let put:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => response;

  let delete:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => response;
};

module Make = (Impl: RequestTypeClass) : FutureTypeClass => {
  type success = (Msg.t, string);
  type error = (option(Msg.t), [ | `response(string) | `error(exn)]);
  type response = Future.t(Belt.Result.t(success, error));

  let respond = (finish, res, data) =>
    res |. Msg.isSuccess ?
      (res, data) |. Belt.Result.Ok |. finish :
      (Some(res), `response(data)) |. Belt.Result.Error |. finish;

  let wrap = request =>
    Future.make(resolver => {
      let onComplete = (msg, data) => respond(resolver, msg, data);
      let onError = (msg, exn) =>
        (Some(msg), `error(exn)) |. Belt.Result.Error |. resolver;

      request
      |. Impl.on(
           `response(res => res |. Msg.read(onComplete, onError) |. ignore),
         )
      |. Impl.on(
           `error(
             exn => (None, `error(exn)) |. Belt.Result.Error |. resolver,
           ),
         );
    });

  let make = (~uri, ~options=?, ~cb=?, _) =>
    Impl.make(~uri, ~options?, ~cb?, ()) |. wrap;

  let get = (~uri, ~options=?, ~cb=?, _) =>
    Impl.get(~uri, ~options?, ~cb?, ()) |. wrap;

  let post = (~uri, ~options=?, ~cb=?, _) =>
    Impl.post(~uri, ~options?, ~cb?, ()) |. wrap;

  let put = (~uri, ~options=?, ~cb=?, _) =>
    Impl.put(~uri, ~options?, ~cb?, ()) |. wrap;

  let delete = (~uri, ~options=?, ~cb=?, _) =>
    Impl.delete(~uri, ~options?, ~cb?, ()) |. wrap;
};
