module Callback = Hyperquest_callback;
module Request = Hyperquest_request;
module Status = Hyperquest_status;

module Buffer: {
  type t;

  let toString: t => string;
};

module Msg: {
  type t;

  let on:
    (
      t,
      [
        | `aborted(unit => unit)
        | `close(unit => unit)
        | `data(Buffer.t => unit)
        | `end_(unit => unit)
        | `error(exn => unit)
      ]
    ) =>
    t;

  let headers: t => Belt.Map.String.t(string);

  let httpVersion: t => string;

  let rawHeaders: t => array(string);

  let rawTrailers: t => array(string);

  let status: t => Status.t;

  let statusCode: t => int;

  let statusMessage: t => string;

  let trailers: t => option(Belt.Map.String.t(string));
};

module Method: {
  type t =
    | Get
    | Post
    | Put
    | Delete
    | Head
    | Options;

  let toString : t => string;

  let fromString : string => Belt.Result.t(t, exn);

  let fromStringUnsafe : string => t;
};

module Options: {
  type t;

  let make:
    (
      ~method: Method.t,
      ~headers: Belt.Map.String.t(string)=?,
      ~agent: string=?,
      ~auth: string=?,
      ~timeout: int=?,
      unit
    ) =>
    t;
};

type t;

let make: (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t;

let get: (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t;

let post: (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t;

let put: (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t;

let delete:
  (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => t;

let on:
  (
    t,
    [
      | `request(Request.t => unit)
      | `response(Msg.t => unit)
      | `error(exn => unit)
    ]
  ) =>
  t;

let finish: t => unit;

let finishCb: (t, ~callback: unit => unit) => unit;

let finishWriteString:
  (
    t,
    string,
    ~encoding: [ | `utf8 | `ascii]=?,
    ~callback: unit => unit=?,
    unit
  ) =>
  unit;

let finishWriteBuffer: (t, Buffer.t, ~callback: unit => unit=?, unit) => unit;

let setHeader: (t, string, string) => t;

let setLocation: (t, string) => t;

let writeString:
  (
    t,
    string,
    ~encoding: [ | `utf8 | `ascii]=?,
    ~callback: unit => unit=?,
    unit
  ) =>
  bool;

let writeBuffer: (t, Buffer.t, ~callback: unit => unit=?, unit) => bool;

module Future: {
  type response =
    Future.t(
      Belt.Result.t(
        (Msg.t, string),
        (option(Msg.t), [ | `response(string) | `error(exn)]),
      ),
    );

  let make:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => response;

  let get:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => response;

  let post:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => response;

  let put:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => response;

  let delete:
    (~uri: string, ~options: Options.t=?, ~cb: Callback.t=?, unit) => response;
};
